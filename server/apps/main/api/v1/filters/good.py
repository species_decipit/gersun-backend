# -*- coding: utf-8 -*-

import re

from django.contrib.postgres.search import SearchVector
from django.db.models import TextField
from django.db.models.expressions import RawSQL
from django.db.models.functions import Cast
from django.db.models.query import Q, QuerySet
from django_filters import FilterSet, CharFilter, BooleanFilter

from server.apps.main.models import Good


def full_text_search(qs: QuerySet, query: str) -> QuerySet:
    q1 = Q(search=query)
    q2 = Q(article__icontains=query)

    qs = qs.annotate(
        search=SearchVector(Cast('characteristic', TextField()))
    ).filter(q1 | q2)

    return qs


class GoodFilter(FilterSet):
    """Class implements filtering with GET params for Good model."""

    article = CharFilter(field_name='article', lookup_expr='icontains')
    category = CharFilter(field_name='category__name', lookup_expr='icontains')
    prev_query = CharFilter(method='search')
    query = CharFilter(method='json_query', field_name='characteristic')
    order_by = CharFilter(
        method='order_by_filter', field_name='characteristic'
    )
    is_sell = BooleanFilter(field_name='is_sell')

    @staticmethod
    def search(queryset: QuerySet, name: str, values: str):
        return full_text_search(queryset, values)

    @staticmethod
    def order_by_filter(queryset: QuerySet, name: str, values: str):
        qs = queryset.order_by(
            RawSQL("characteristic->%s", (re.sub(r'^-', '', values),))
        )
        if values.startswith('-'):
            return qs.reverse()
        else:
            return qs

    @staticmethod
    def json_query(queryset: QuerySet, name: str, values: str):
        """Method performs JSONPath query to 'characteristic' field."""
        accumulated_query = Q()
        value: str
        for value in values.split('&'):
            lookup, query = value.strip().split('=')

            if query.isdigit():
                query = int(query)
            elif query.startswith('['):
                query = [
                    word.strip()
                    for word in re.sub(r'[[\[\]]', '', query).split(',')
                ]
            elif query in ['true', 'false']:
                query = True if query == 'true' else False

            accumulated_query &= Q(
                **{'characteristic__{0}'.format(lookup): query}
            )
        return queryset.filter(accumulated_query)

    class Meta(object):
        model = Good
        fields = ()
