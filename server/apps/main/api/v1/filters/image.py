# -*- coding: utf-8 -*-

from django_filters import FilterSet, NumberFilter

from server.apps.main.models import Image


class ImageFilter(FilterSet):
    category = NumberFilter(field_name='category')

    class Meta(object):
        model = Image
        fields = ()
