# -*- coding: utf-8 -*-

from django.urls import include, path
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from server.apps.main.api.v1.views.category import CategoryView, CategoryViewAdmin
from server.apps.main.api.v1.views.gallery import GalleryView, GalleryViewAdmin
from server.apps.main.api.v1.views.good import GoodView, GoodViewAdmin
from server.apps.main.api.v1.views.image import ImageView, ImageViewAdmin, ImageCategoryViewAdmin

user_router = routers.SimpleRouter()
admin_router = routers.SimpleRouter()

user_router.register(
    prefix=r'categories',
    viewset=CategoryView,
    basename='category',
)
user_router.register(
    prefix=r'goods',
    viewset=GoodView,
    basename='good',
)
user_router.register(
    prefix=r'images',
    viewset=ImageView,
    basename='image',
)
user_router.register(
    prefix=r'galleries',
    viewset=GalleryView,
    basename='gallery',
)

admin_router.register(prefix=r'categories', viewset=CategoryViewAdmin, basename='category')
admin_router.register(prefix=r'galleries', viewset=GalleryViewAdmin, basename='gallery')
admin_router.register(prefix=r'goods', viewset=GoodViewAdmin, basename='good')
admin_router.register(prefix=r'images', viewset=ImageViewAdmin, basename='image')
admin_router.register(
    prefix=r'image_categories',
    viewset=ImageCategoryViewAdmin,
    basename='image_categories',
)


urlpatterns = [
    path('', include(user_router.urls)),
    path('admin/', include(admin_router.urls)),
    path('login/', obtain_auth_token, name='api_token_auth'),
]
