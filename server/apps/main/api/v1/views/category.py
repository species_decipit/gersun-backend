# -*- coding: utf-8 -*-

from typing import Any, Dict, List

from django.db.models.query import QuerySet
from rest_framework.permissions import IsAdminUser
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.viewsets import ModelViewSet

from server.apps.main.api.v1.serializers.category import (
    CategorySerializer,
    CharacteristicSerializer,
)
from server.apps.main.models import Category, Good, Characteristic


def create_bound_values(queryset: QuerySet) -> Dict[str, Any]:
    characteristics_field_name = 'characteristics'
    categories: List[Dict[str, Any]] = []  # type: ignore

    category: Category
    for category in Category.objects.all():
        categories.append(CategorySerializer(category).data)
        categories[-1][characteristics_field_name] = []
        characteristic: Characteristic
        for characteristic in category.characteristics.filter(show_filter=True):
            distinct_values = (
                queryset.filter(
                    category=category,
                    **{'characteristic__{0}__isnull'.format(characteristic.name): False},
                )
                .exclude(**{'characteristic__{0}'.format(characteristic.name): None})
                .values_list(
                    'characteristic__{0}'.format(characteristic.name),
                    flat=True,
                )
                .distinct()
            )
            categories[-1][characteristics_field_name].append(
                {
                    **CharacteristicSerializer(characteristic).data,
                    'values': distinct_values,
                }
            )
            if characteristic.data_type == characteristic.number and distinct_values:
                categories[-1][characteristics_field_name][-1]['values'] = {
                    'max': max(map(int, distinct_values)),
                    'min': min(map(int, distinct_values)),
                }
    postfixes: List[str] = ['gt', 'gte', 'lt', 'lte', 'icontains', 'in']

    return {'postfixes': postfixes, 'categories': categories}


class CategoryView(ModelViewSet):
    """View is responsible for retrieving Categories."""

    def list(self, request: Request, *args, **kwargs) -> Response:
        """Method adds to list of categories bounds parameters."""

        return Response(data=create_bound_values(Good.objects.all()), status=HTTP_200_OK)

    http_method_names = ('get',)
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class CategoryViewAdmin(ModelViewSet):
    permission_classes = (IsAdminUser,)
    http_method_names = ('get', 'delete', 'post', 'patch', 'options')
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
