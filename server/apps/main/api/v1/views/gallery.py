# -*- coding: utf-8 -*-
from django.db.models.query import QuerySet
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet

from server.apps.main.api.v1.serializers.gallery import GallerySerializer
from server.apps.main.models import Gallery


class GalleryView(ModelViewSet):
    http_method_names = ('get',)
    serializer_class = GallerySerializer

    def get_queryset(self) -> QuerySet:
        return Gallery.objects.filter(is_show=True).order_by('order')


class GalleryViewAdmin(ModelViewSet):
    permission_classes = (IsAdminUser,)
    http_method_names = ('get', 'delete', 'post', 'patch', 'options')
    serializer_class = GallerySerializer
    queryset = Gallery.objects.all()
