# -*- coding: utf-8 -*-
from django.db.models.query import QuerySet
from rest_framework.permissions import IsAdminUser
from rest_framework.viewsets import ModelViewSet

from server.apps.main.api.v1.filters.image import ImageFilter
from server.apps.main.api.v1.serializers.image import (
    ImageSerializer,
    ImageAdminSerializer,
    ImageCategoryAdminSerializer,
)
from server.apps.main.models import Image, ImageCategory


class ImageView(ModelViewSet):
    http_method_names = ('get',)
    serializer_class = ImageSerializer
    filter_class = ImageFilter

    def get_queryset(self) -> QuerySet:
        return Image.objects.all().order_by('order')


class ImageViewAdmin(ModelViewSet):
    permission_classes = (IsAdminUser,)
    http_method_names = ('get', 'delete', 'post', 'patch', 'options')
    serializer_class = ImageAdminSerializer
    queryset = Image.objects.all()


class ImageCategoryViewAdmin(ModelViewSet):
    permission_classes = (IsAdminUser,)
    http_method_names = ('get', 'delete', 'post', 'patch', 'options')
    serializer_class = ImageCategoryAdminSerializer
    queryset = ImageCategory.objects.all()
