# -*- coding: utf-8 -*-


from rest_framework.decorators import action
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.viewsets import ModelViewSet

from server.apps.main.api.v1.filters.good import GoodFilter, full_text_search
from server.apps.main.api.v1.serializers.good import GoodSerializer, GoodSerializerAdmin
from server.apps.main.api.v1.views.category import create_bound_values
from server.apps.main.models import Good


class GoodView(ModelViewSet):
    """View is responsible for retrieving (with filters) Good."""

    http_method_names = ('get',)
    serializer_class = GoodSerializer
    queryset = (
        Good.objects.filter(is_show=True)
        .select_related('category')
        .prefetch_related('images')
        .prefetch_related('category__characteristics')
        .all()
    )
    filter_class = GoodFilter

    @action(detail=False, methods=['GET'], name='Search')
    def search(self, request, *args, **kwargs):
        query = request.GET.get('query', None)
        order_by = request.GET.get('order_by', None)
        order_by = order_by if order_by else 'объем'
        reverse = order_by.startswith('-')
        order_by = order_by[1:] if order_by.startswith('-') else order_by

        qs = Good.objects.prefetch_related('category__characteristics', 'images')

        if query:
            result = GoodSerializer(
                full_text_search(qs, query),
                many=True,
                context={'request': request},
            ).data
        else:
            result = GoodSerializer(qs, many=True, context={'request': request}).data

        result_to_sort = filter(lambda row: order_by in row['characteristic'], result)
        sorted_result = list(
            sorted(
                result_to_sort, key=lambda row: float(row['characteristic'][order_by].split(' ')[0]), reverse=reverse
            )
        )
        articles = {row['article'] for row in sorted_result}

        unsorted_result = [row for row in result if row['article'] not in articles]

        return Response(
            {'bounds': create_bound_values(qs), 'result': sorted_result + unsorted_result},
            status=HTTP_200_OK,
        )


class GoodViewAdmin(ModelViewSet):
    permission_classes = (IsAdminUser,)
    http_method_names = ('get', 'delete', 'post', 'patch', 'options')
    serializer_class = GoodSerializerAdmin
    queryset = Good.objects.all()
