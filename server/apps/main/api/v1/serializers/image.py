# -*- coding: utf-8 -*-

from rest_framework.serializers import ModelSerializer

from server.apps.main.models import Image, ImageCategory


class ImageSerializer(ModelSerializer):
    """Default serializer for Image instance."""

    class Meta(object):
        model = Image
        fields = ('image', 'name')


class ImageAdminSerializer(ModelSerializer):
    """Image serializer for Admin Dashboard"""

    class Meta(object):
        model = Image
        fields = ('id', 'image', 'name', 'category', 'order')


class ImageCategoryAdminSerializer(ModelSerializer):
    """Image category serializer for Admin Dashboard"""

    class Meta(object):
        model = ImageCategory
        fields = ('id', 'name')
