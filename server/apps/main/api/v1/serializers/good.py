# -*- coding: utf-8 -*-

from typing import List, Dict, Any

from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    PrimaryKeyRelatedField,
    ListSerializer,
    ImageField,
)

from server.apps.main.api.v1.serializers.category import CategorySerializer
from server.apps.main.api.v1.serializers.characteristic import (
    CharacteristicSerializer,
)
from server.apps.main.api.v1.serializers.image import ImageSerializer
from server.apps.main.models import Good, Category, Image


class GoodSerializer(ModelSerializer):
    """Default serializer for Good instance."""

    characteristic = SerializerMethodField()
    images = SerializerMethodField()

    def get_images(self, instance: Good) -> List[Dict]:
        return [
            image['image']
            for image in ImageSerializer(instance.images, many=True, context=self.context).data
        ]

    @staticmethod
    def get_characteristic(instance: Good) -> Dict:
        result: Dict = {}
        characteristics = CharacteristicSerializer(
            instance.category.characteristics,
            many=True,
        ).data
        for key, value in instance.characteristic_resolved.items():
            measurement_unit = next(
                filter(
                    lambda ch: ch['name'] == key and ch['measurement_unit'],
                    characteristics,
                ),
                None,
            )
            if measurement_unit:
                result[key] = '{0} {1}'.format(str(value), measurement_unit['measurement_unit'])
            else:
                result[key] = value

        return result

    class Meta(object):
        model = Good
        fields = (
            'article',
            'category',
            'images',
            'characteristic',
            'video',
            'model',
            'is_sell',
        )


class GoodSerializerAdmin(ModelSerializer):
    category = CategorySerializer(read_only=True)
    category_id = PrimaryKeyRelatedField(write_only=True, queryset=Category.objects.all())
    images = ListSerializer(child=ImageField(), write_only=True)

    def create(self, validated_data: Dict[str, Any]) -> Dict[str, Any]:
        validated_data['characteristic'] = {
            key: int(value) if isinstance(value, str) and value.isnumeric() else value
            for key, value in validated_data['characteristic'].items()
        }
        images = validated_data.pop('images')
        validated_data['category_id'] = validated_data['category_id'].id
        good = super().create(validated_data)
        for index, image in enumerate(images):
            good.images.add(Image.objects.create(image=image, order=index))
        return good

    def update(self, instance: Good, validated_data: Dict[str, Any]) -> Good:
        previous_article = instance.article
        if validated_data.get('characteristic', None):
            validated_data['characteristic'] = {
                key: int(value) if isinstance(value, str) and value.isnumeric() else value
                for key, value in validated_data['characteristic'].items()
            }
        images = validated_data.pop('images', None)
        good = super().update(instance, validated_data)
        if previous_article != good.article:
            Good.objects.filter(article=previous_article).delete()
        if images:
            good.images.all().delete()
            for index, image in enumerate(images):
                good.images.add(Image.objects.create(image=image, order=index))
        return good

    def to_representation(self, instance: Good) -> Dict[str, Any]:
        result = super().to_representation(instance)
        result['images'] = ImageSerializer(
            instance.images.order_by('order'),
            many=True,
            context=self.context,
        ).data
        result['characteristic'] = {
            characteristic: value
            for characteristic, value in result['characteristic'].items()
            if instance.category.characteristics.filter(name=characteristic).exists()
        }

        return result

    class Meta(object):
        model = Good
        fields = (
            'article',
            'category',
            'category_id',
            'images',
            'characteristic',
            'video',
            'model',
            'is_sell',
            'is_show',
        )
