# -*- coding: utf-8 -*-
from typing import Dict, Any

from rest_framework.serializers import ModelSerializer

from server.apps.main.api.v1.serializers.characteristic import (
    CharacteristicSerializer,
)
from server.apps.main.models import Category


class CategorySerializer(ModelSerializer):
    """Default serializer for Category instance."""

    characteristics = CharacteristicSerializer(many=True)

    def create(self, validated_data: Dict[str, Any]) -> Category:
        category = Category.objects.create(name=validated_data['name'])
        for characteristic in validated_data['characteristics']:
            CharacteristicSerializer().create(
                {
                    **characteristic,
                    'category_id': category.id,
                },
            )
        return category

    def update(self, instance: Category, validated_data: Dict[str, Any]) -> Category:
        characteristics = validated_data.pop('characteristics', None)
        category = super().update(instance, validated_data)
        if characteristics:
            category.characteristics.all().delete()
            for characteristic in characteristics:
                CharacteristicSerializer().create(
                    {
                        **characteristic,
                        'category_id': category.id,
                    },
                )
        return category

    class Meta(object):
        model = Category
        fields = ('id', 'name', 'characteristics')
