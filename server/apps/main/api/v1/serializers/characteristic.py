# -*- coding: utf-8 -*-

from rest_framework.serializers import ModelSerializer

from server.apps.main.models import Characteristic


class CharacteristicSerializer(ModelSerializer):
    class Meta(object):
        model = Characteristic
        fields = ('id', 'name', 'data_type', 'measurement_unit', 'category_id')
