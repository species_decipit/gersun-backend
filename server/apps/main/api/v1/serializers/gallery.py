# -*- coding: utf-8 -*-

from rest_framework.serializers import ModelSerializer

from server.apps.main.models import Gallery


class GallerySerializer(ModelSerializer):
    """Default serializer for Gallery instance."""

    class Meta(object):
        model = Gallery
        fields = ('id', 'title', 'link', 'is_show', 'order')
