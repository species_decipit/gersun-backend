from django import forms

from server.apps.main.models import Characteristic


class ChangeCharacteristicForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    characteristic = forms.CharField(label='Хар-ка товара')
    new_value = forms.CharField(label='Новое значение')
