# -*- coding: utf-8 -*-

from django.urls import include, path

urlpatterns = [
    path('api/v1/', include('server.apps.main.api.v1.urls')),
]
