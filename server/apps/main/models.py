# -*- coding: utf-8 -*-

from django.contrib.postgres.fields import JSONField
from django.db.models import (
    PROTECT,
    CharField,
    ForeignKey,
    ImageField,
    ManyToManyField,
    Model,
    BooleanField,
    IntegerField,
    FileField,
    CASCADE,
    PositiveIntegerField,
)


class Category(Model):
    """Model represents category of the goods."""

    name = CharField(
        max_length=64,
        verbose_name='Наименование категории',
        help_text='Название категории',
    )

    def __str__(self) -> str:
        """Method return string representations of Category instance."""
        return '{0}. {1}'.format(self.id, self.name)

    class Meta(object):
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Characteristic(Model):
    """Model represents characteristic (field) of the good`s category."""

    string = 'Строка'
    number = 'Число'
    boolean = 'Да/Нет'

    choices = (
        (string, string),
        (number, number),
        (boolean, boolean),
    )

    name = CharField(
        max_length=64,
        verbose_name='Харастеристика товара',
        help_text='Харастеристика товара (поле)',
    )
    data_type = CharField(
        max_length=64,
        choices=choices,
        verbose_name='Тип данных в поле',
        help_text='Тип данных в поле',
    )
    measurement_unit = CharField(
        max_length=64,
        verbose_name='Единица измерения характеристика',
        help_text='Единица измерения характеристика',
        null=True,
        blank=True,
    )
    show_filter = BooleanField(
        default=True,
        verbose_name='Показывать фильтр для поля',
        help_text='Если Да, то фильтр будет показываться, иначе нет',
    )
    category = ForeignKey(
        Category,
        on_delete=PROTECT,
        related_name='characteristics',
        verbose_name='Категория товара',
        help_text='Категория товара к которой относится поле (харастеристика)',
    )

    def __str__(self) -> str:
        """Method return string representations of Characteristic instance."""
        return '{0} - {1}'.format(self.category.name, self.name)

    class Meta(object):
        verbose_name = 'Характеристика категории товара'
        verbose_name_plural = 'Характеристики категории товара'


class CharacteristicOption(Model):
    """Model represents characteristic option (select) of the good`s category."""

    value = CharField(
        max_length=64,
        verbose_name='Опция',
        help_text='Опция харастеристики товара (поле)',
    )
    characteristic = ForeignKey(Characteristic, related_name='options', on_delete=CASCADE)

    def __str__(self) -> str:
        """Method return string representations of Characteristic instance."""
        return '{0} - {1}'.format(self.category.name, self.name)

    class Meta(object):
        verbose_name = 'Опция характеристики категории товара'
        verbose_name_plural = 'Опции характеристики категории товара'


class ImageCategory(Model):
    """Model represents category of images."""

    name = CharField(
        max_length=64,
        verbose_name='Категория изображения',
        help_text='Название категории изображения',
    )

    def __str__(self) -> str:
        """Method return string representations of ImageCategory instance."""
        return '{0}. {1}'.format(self.id, self.name)

    class Meta(object):
        verbose_name = 'Категория изображения'
        verbose_name_plural = 'Категории изображений'


class Image(Model):
    """Model represents pictures of the goods."""

    image = ImageField(
        max_length=512,
        verbose_name='Изображение',
        help_text='Изображение товара',
    )
    name = CharField(
        max_length=128,
        verbose_name='Подпись изображения',
        help_text='Подпись, которая будет выводиться для изображения',
        null=True,
        blank=True,
    )
    category = ForeignKey(
        ImageCategory,
        on_delete=PROTECT,
        related_name='images',
        verbose_name='Категория изображения',
        help_text='К какой категории относится изображение',
        null=True,
    )
    order = IntegerField(
        default=0,
        verbose_name='Номер картинки',
        help_text='Порядок картинки для вывода',
    )

    def __str__(self) -> str:
        """Method return string representations of Image instance."""
        return (
            f'{self.id}. {self.category} - {self.name} '
            f'- {list(self.goods.values_list("article", flat=True))}'
        )

    class Meta(object):
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'


class Good(Model):
    """Models represents goods."""

    article = CharField(
        primary_key=True,
        max_length=128,
        verbose_name='Артикул',
        help_text='Артикул товара',
    )
    category = ForeignKey(
        Category,
        on_delete=PROTECT,
        related_name='goods',
        verbose_name='Категория',
        help_text='Категория товара',
    )
    images = ManyToManyField(
        Image,
        related_name='goods',
        verbose_name='Изображения',
        help_text='Изображения товара',
        blank=True,
    )
    characteristic = JSONField(
        'Характеристика товара',
        help_text='Словарь с характеристиками товара',
        default=dict,
    )
    is_show = BooleanField('Показывать ли товар в каталоге?', default=True)

    @property
    def characteristic_resolved(self):
        characteristic = self.characteristic
        out = {}
        for k, v in characteristic.items():
            if isinstance(v, int):
                option = CharacteristicOption.objects.filter(
                    pk=v, characteristic__name=characteristic
                ).first()
                if option:
                    out[k] = option.value
                else:
                    out[k] = v
            else:
                out[k] = v
        return out

    is_sell = BooleanField(default=False, verbose_name='Акционный товар')
    video = FileField(verbose_name='Видео', null=True, blank=True)
    model = FileField(verbose_name='3D модель', null=True, blank=True)

    def __str__(self) -> str:
        """Method return string representations of Model instance."""
        return '{0}'.format(self.article)

    class Meta(object):
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class Gallery(Model):
    title = CharField(max_length=128)
    link = CharField(max_length=1024)
    is_show = BooleanField()
    order = PositiveIntegerField(default=0)

    def __str__(self) -> str:
        return f'{self.id}. {self.title}'

    class Meta:
        ordering = ('order',)
        verbose_name = 'Галерея'
        verbose_name_plural = 'Галереи'
