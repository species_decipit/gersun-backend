# -*- coding: utf-8 -*-
from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from django.contrib.postgres import fields
from django.forms import CharField, ModelForm, ChoiceField
from django_json_widget.widgets import JSONEditorWidget
from jet.admin import CompactInline

from server.apps.main.models import (
    Category,
    Good,
    Image,
    ImageCategory,
    Characteristic,
    CharacteristicOption,
    Gallery,
)

admin.site.register(Category)
admin.site.register(ImageCategory)


class ImageAdmin(admin.ModelAdmin):
    list_filter = ('category',)
    ordering = ('name',)


admin.site.register(Image, ImageAdmin)


class GoodsDynamicFieldsForm(ModelForm):
    class Meta:
        model = Good
        exclude = ['characteristic']

    def __init__(self, *args, **kwargs):
        self.previous_article = kwargs['instance'].article
        super().__init__(*args, **kwargs)
        # The following dynamically add three fields to the model form
        if 'instance' not in kwargs:
            return
        good = kwargs['instance']
        if good is None: return
        fields_list = Characteristic.objects.filter(category__exact=good.category)
        for x in fields_list:
            options = x.options.all()
            if x.data_type == Characteristic.boolean:
                choices = [
                    ('да', 'да'),
                    ('нет', 'нет')
                ]
                self.base_fields[x.name] = ChoiceField(
                    initial=good.characteristic.get(x.name, ''), choices=choices
                )
                self.fields[x.name] = ChoiceField(
                    initial=good.characteristic.get(x.name, ''), choices=choices
                )
            # if characteristic with options
            elif x.options.all().count() > 0:
                choices = []
                for ch in options:
                    choices.append((ch.pk, ch.value))
                self.base_fields[x.name] = ChoiceField(
                    initial=good.characteristic.get(x.name, ''), choices=choices
                )
                self.fields[x.name] = ChoiceField(
                    initial=good.characteristic.get(x.name, ''), choices=choices
                )
            # if no options
            else:
                self.base_fields[x.name] = CharField(
                    initial=good.characteristic.get(x.name, ''), required=False
                )
                self.fields[x.name] = CharField(
                    initial=good.characteristic.get(x.name, ''), required=False
                )

    def save(self, *args, **kwargs):
        fields_list = Characteristic.objects.filter(category__exact=self.instance.category)
        characteristic = dict()
        for x in fields_list:
            options_exist = x.options.all().count() > 0
            if options_exist:
                v = self.cleaned_data.get(x.name)
                characteristic[x.name] = int(v) if v else v
            elif x.data_type == Characteristic.number:
                v = self.cleaned_data.get(x.name, 0.0)
                characteristic[x.name] = float(v) if v else v
            else:
                characteristic[x.name] = self.cleaned_data.get(x.name, '')
        self.instance.characteristic = characteristic
        self.instance = super().save(*args, **kwargs)
        if self.previous_article != self.instance.article:
            Good.objects.filter(article=self.previous_article).delete()
        return self.instance

    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget},
    }


class GoodAdmin(admin.ModelAdmin):
    def get_form(self, request, obj=None, change=False, **kwargs):
        return GoodsDynamicFieldsForm


class CharacteristicOptionInline(CompactInline):
    model = CharacteristicOption
    extra = 1
    model.__str__ = lambda x: str(x.value)
    verbose_name = "Опция"
    verbose_name_plural = "Опции"


class CharacteristicAdmin(admin.ModelAdmin):
    inlines = (CharacteristicOptionInline,)


admin.site.register(Good, GoodAdmin)
admin.site.register(Characteristic, CharacteristicAdmin)


@admin.register(Gallery)
class GalleryAdmin(SortableAdminMixin, admin.ModelAdmin):
    pass
