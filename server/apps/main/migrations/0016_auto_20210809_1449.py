# Generated by Django 2.2.24 on 2021-08-09 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_auto_20210809_1223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='characteristicoption',
            name='value',
            field=models.CharField(help_text='Опция харастеристики товара (поле)', max_length=64, verbose_name='Опция'),
        ),
    ]
